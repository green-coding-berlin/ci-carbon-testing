# CI Carbon Testing

This repository is a running sample integration for the Gitlab functionality of Eco-CI.

Eco-CI is a carbon estimation plugin for CI/CD pipelines that can show the estimated energy consumption and CO2 emissions. It utilizes a machine learning model based on the SPECpower data and CO2 grid intensity data from Electricitymaps.

## Functionality
It will run the pipeline once a week to validate that the plugin still works


## Links to the project:
- [Eco-CI info page](https://www.green-coding.io/projects/eco-ci/)
- [Eco-CI Github page](https://github.com/green-coding-berlin/eco-ci-energy-estimation)
- [Cloud Energy ML Model](https://www.green-coding.io/projects/cloud-energy/)
